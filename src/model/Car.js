export default class Car {
	constructor(marka, model, przebieg, rok) {
		this.marka = marka;
		this.model = model;
		this.przebieg = przebieg;
		this.rok = rok;
	}
	getCarTitle() {
		return this.marka +' '+ this.model;
	}
}