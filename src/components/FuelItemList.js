import React, { Component } from 'react';
import { View, Text, StyleSheet} from 'react-native';

export default class FuelItemList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			stanLicznika: props.stanLicznika,
			ileLitrow: props.ileLitrow,
			cena: props.cena,
			cenaL: props.cenaL
		};
	}
	
	render() {
		return (
			<View style={style.container}>
			<View style={style.blueDot}/>
			
			<Text style={style.date}>10.10.2018 9:54</Text>
			<View style={style.content}> 
				<View style={style.row}>
					<Text style={style.text1}>STAN LICZNIKA</Text>
					<Text>{this.state.stanLicznika}km</Text>
				</View>
				<View style={style.row}>
					<Text style={style.text1}>ILE LITRÓW</Text>
					<Text>{this.state.ileLitrow}l</Text>
				</View>
				<View style={style.row}>
					<Text style={style.text1}>CENA</Text>
					<Text>{this.state.cena}zł</Text>
				</View>
				<View style={style.row}>
					<Text style={style.text1}>CENA ZA LITR</Text>
					<Text>{this.state.cenaL}zł</Text>
				</View>
			</View>
			</View>
			);
		}
	};
	const style = StyleSheet.create({
		container: {
			paddingHorizontal: 16,
		},
		blueDot: {
			marginLeft: -6,
			width: 14,
			height: 14,
			backgroundColor: '#0085FF',
			borderRadius: 7,
		},
		content: {
			marginTop: -2,
			paddingHorizontal: 8,			
			borderLeftColor: '#808080',
			borderLeftWidth: 2,
			paddingBottom: 16,
		}, 
		date: {
			marginTop: -18,
			marginLeft: 16,
			fontStyle: 'italic',
			color: '#B7B7B7'
		}, 
		row: {
			flexDirection: 'row',
			justifyContent: 'space-between',
		},
		text1:
		{
			fontWeight: '700', fontSize: 12,
		}
	});