import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button, Label, Input, Item, Form } from "native-base";
import Car from "./model/Car";
import { Actions } from "react-native-router-flux";

const Realm = require("realm");

export default class AddCarScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      marka: "",
      model: "",
      rok: "",
      przebieg: ""
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.headerText}>Dodaj nowy pojazd</Text>
        <View style={styles.content}>
          <Text
            style={{
              color: "#000",
              fontSize: 24,
              textAlign: "center",
              fontWeight: "bold",
              padding: 16
            }}
          >
            Wypełnij pola i dodaj pojazd
          </Text>
          <Form>
            <Item floatingLabel>
              <Label>Marka</Label>
              <Input
                value={this.state.marka}
                onChangeText={text => {
                  this.setState({ marka: text });
                }}
              />
            </Item>

            <Item floatingLabel>
              <Label>Model</Label>
              <Input
                value={this.state.model}
                onChangeText={text => {
                  this.setState({ model: text });
                }}
              />
            </Item>

            <Item floatingLabel>
              <Label>Obecny przebieg</Label>
              <Input
                value={this.state.przebieg}
                onChangeText={text => {
                  this.setState({ przebieg: text });
                }}
              />
            </Item>

            <Item floatingLabel>
              <Label>Rok produkcji</Label>
              <Input
                value={this.state.rok}
                onChangeText={text => {
                  this.setState({ rok: text });
                }}
              />
            </Item>

            <Button
              rounded
              style={{
                backgroundColor: "#0085FF",
                width: 70 + "%",
                alignSelf: "center",
                margin: 16
              }}
              onPress={() => {
                let car = new Car(
                  this.state.marka,
                  this.state.model,
                  this.state.przebieg,
                  this.state.rok
                );
                Realm.open({
                  schema: [
                    {
                      name: "Cars",
                      properties: {
                        marka: "string",
                        model: "string",
                        przebieg: "string",
                        rok: "string"
                      }
                    }
                  ]
                }).then(realm => {
					realm.write(() =>
					realm.create('Cars', car));
                });
				Actions.push('home', car);
			  }}
            >
              <Text
                style={{
                  color: "#fff",
                  fontWeight: "bold",
                  textAlign: "center",
                  width: 100 + "%",
                  fontSize: 18
                }}
              >
                Dodaj pojazd
              </Text>
            </Button>
          </Form>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0085FF"
  },

  headerText: {
    fontSize: 24,
    color: "#fff",
    padding: 16,
    textAlign: "center"
  },
  content: {
    backgroundColor: "#fff",
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    flex: 1,
    paddingVertical: 8
  }
});
