import React, { Component } from "react";
import { View, Text, StyleSheet, Button, FlatList, Alert } from "react-native";
import { Actions } from "react-native-router-flux";
import Car from "./model/Car";
import FuelItemList from "./components/FuelItemList";

const Realm = require("realm");

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      car: null,
      data: [1, 2, 3],
      realm: null
    };
  }

  componentDidMount() {
    //TODO do naprawienia realm i przechodzenie do AddCarScreen
    Realm.open({
      schema: [
        {
          name: "Cars",
          properties: {
            marka: "string",
            model: "string",
            przebieg: "string",
            rok: "string"
          }
        }
      ]
    }).then(realm => {
      if (realm.objects("Cars").length < 1) Actions.jump("addcar");
      this.setState({ car: realm.objects("Cars")[0] });
      this.setState({ realm: realm });
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.headerText}>
          {this.state.car ? (
            <Text>{this.state.car.marka + " " + this.state.car.model}</Text>
          ) : (
            <Text>Ładowanie...</Text>
          )}{" "}
        </Text>
        <View style={styles.content}>
          <Text style={{ textAlign: "center", fontWeight: "800" }}>
            Oś czasu
          </Text>
          <FlatList
            style={{ alignContent: "center" }}
            data={this.state.data}
            renderItem={({ item }) => (
              <FuelItemList
                stanLicznika="2000"
                ileLitrow="30"
                cena="220.22"
                cenaL="4.55"
              />
            )}
          />
          <View style={styles.buttonContainer}>
            <Button
              title="Dodaj tankowanie"
              onPress={() => {
                Actions.jump("addcar");
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0085FF"
  },
  headerText: {
    fontSize: 24,
    color: "#fff",
    padding: 8
  },
  content: {
    backgroundColor: "#fff",
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    flex: 1,
    paddingVertical: 8
  },
  buttonContainer: {
    width: 70 + "%",
    alignSelf: "center",
    position: "absolute",
    bottom: 16
  },
  buttonStyle: {}
});
