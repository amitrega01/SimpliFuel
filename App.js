import React, { Component } from "react";
import { Stack, Scene, Router } from "react-native-router-flux";
import HomeScreen from "./src/HomeScreen";
import AddCarScreen from "./src/AddCarScreen";
import AddItem from "./src/AddItem";

export default class App extends Component {
  render() {
    return (
      <Router>
        <Scene key="root" hideNavBar>
          <Scene key="home" component={HomeScreen} title="SimpliFuel" initial />
          <Scene key="addcar" component={AddCarScreen} title="Dodaj pojazd" />
          <Scene key="additem" component={AddItem} title="Dodaj tankowanie" />
        </Scene>
      </Router>
    );
  }
}
